module DrawCircleHull exposing (..)

import Array

import Browser
import Browser.Dom as Dom
import Browser.Events as E

import Html as Ht
import Html.Attributes as H
import Html.Events as He

import Random

import Svg exposing (..)
import Svg.Attributes as A exposing (..)
import Json.Decode as D

import Task
import Time

main =
    Browser.document
        { init = initial
        , update = update
        , view = view
        , subscriptions = subscriptions }

type Msg = NewViewport Dom.Viewport
         | Resize Int Int
         | MouseMove Float Float
         | MouseButton Bool
         | Tick Time.Posix
         | Clear
         | Play
         | Slide String

type alias Screen =
    { width : Float
    , height : Float
    , top : Float
    , bottom : Float
    , left : Float
    , right : Float }

toScreen w h =
    { width = w
    , height = h
    , top = h / 2
    , bottom = -h / 2
    , left = -w / 2
    , right = w / 2 }

type alias Mouse =
    { x : Float
    , y : Float
    , down : Bool }

initMouse =
    { x = 0
    , y = 0
    , down = False }

type alias Circle =
    { did : Int
    , center : Point
    , radius : Float
    , dest : Maybe VectDir}

type alias Point =
    { x : Float
    , y : Float }

type alias Line =
    { p1 : Point
    , p2 : Point }

type Orientation = Col
                 | Ccw
                 | Cw

orientation : Point -> Point -> Point -> Orientation
orientation p q r =
    let
        value = (r.y - p.y) * (q.x - p.x) - (q.y - p.y) * (r.x - p.x)
    in
        if value == 0 then Col
        else if value > 0 then Ccw
             else Cw

lineOrientation : Line -> Point -> Orientation
lineOrientation l p = orientation l.p1 l.p2 p

lineDistance : Line -> Float
lineDistance l = distance l.p1 l.p2

type alias VectDir =
    { p : Point
    , dx : Float
    , dy : Float}

lineToDir : Line -> VectDir
lineToDir l = { p = l.p1, dx = l.p2.x - l.p1.x, dy = l.p2.y - l.p1.y }

dirToLine : VectDir -> Line
dirToLine vect = { p1 = vect.p, p2 = { x = vect.p.x + vect.dx, y = vect.p.y + vect.dy } }

normalize : VectDir -> VectDir
normalize vect =
    let
        norm = sqrt <| vect.dx^2 + vect.dy^2
    in
        { vect | dx = vect.dx / norm, dy = vect.dy / norm }

dirAngle : VectDir -> VectDir -> Float
dirAngle v1 v2 =
    let
        angle = acos (pointProd / (normV1 * normV2))
        pointProd = v1.dx * v2.dx + v1.dy * v2.dy
        normV1 = sqrt <| v1.dx^2 + v1.dy^2
        normV2 = sqrt <| v2.dx^2 + v2.dy^2
    in
        if sameDir v1 v2 then 0
        else if orientation { x = 0, y = 0} { x = v1.dx, y = v1.dy } { x = v2.dx, y = v2.dy } == Ccw then 2 * pi - angle
             else angle

sameDir : VectDir -> VectDir -> Bool
sameDir v1 v2 =
    let
        normalV1 = normalize v1
        normalV2 = normalize v2
    in
        normalV1.dx == normalV2.dx && normalV1.dy == normalV2.dy

outterTangents : Circle -> Circle -> Maybe (Line, Line)
outterTangents discA discB =
    let
        hyp = lineDistance {p1 = discA.center, p2 = discB.center}
        r = abs <| discA.radius - discB.radius
        phi1 = atan2 (discB.center.y - discA.center.y) (discB.center.x - discA.center.x) + acos (r / hyp)
        t1x = discA.center.x + discA.radius * cos phi1
        t1y = discA.center.y + discA.radius * sin phi1
        p1 = { x = t1x, y = t1y }
        t2x = discB.center.x + discB.radius * cos phi1
        t2y = discB.center.y + discB.radius * sin phi1
        p2 = { x = t2x, y = t2y }
        line1 = { p1 = p1, p2 = p2 }
        phi2 = atan2 (discB.center.y - discA.center.y) (discB.center.x - discA.center.x) - acos (r / hyp)
        t3x = discA.center.x + discA.radius * cos phi2
        t3y = discA.center.y + discA.radius * sin phi2
        p3 = { x = t3x, y = t3y }
        t4x = discB.center.x + discB.radius * cos phi2
        t4y = discB.center.y + discB.radius * sin phi2
        p4 = { x = t4x, y = t4y}
        line2 = { p1 = p3, p2 = p4 }
    in
        if discA.radius < discB.radius then outterTangents discB discA
        else if discA.radius >= hyp + discB.radius then Nothing
             else Just (line1, line2)

tangentFromCircleToCircle : Circle -> Circle -> Maybe Line
tangentFromCircleToCircle discA discB =
    case outterTangents discA discB of
        Nothing -> Nothing
        Just (l1, l2) -> if discA.radius < discB.radius then
                             let
                                 fun line = { p1 = line.p2, p2 = line.p1 }
                                 (fl1, fl2) = (fun l1, fun l2)
                             in
                                 case lineOrientation fl2 fl1.p2 of
                                     Cw -> Just fl2
                                     _ -> Just fl1
                         else case lineOrientation l2 (l1.p2) of
                                  Cw -> Just l2
                                  _ -> Just l1


perpFromCircle : VectDir -> Circle -> VectDir
perpFromCircle vect disc =
    let
        (x1, y1) = (vect.p.x, vect.p.y)
        (cx, cy) = (disc.center.x, disc.center.y)
        x = (cy - y1 + m * x1 + mi * cx) / (m + mi)
        m = vect.dy / vect.dx
        mi = vect.dx / vect.dy
        y = m * (x-x1) + y1
    in
        if vect.dx == 0 then { p = disc.center, dx = if x1 < cx then (-disc.radius) else disc.radius, dy = 0 }
        else if vect.dy == 0 then { p = disc.center, dx = 0, dy = if y1 < cy then (-disc.radius) else disc.radius }
             else { p = disc.center, dx = x - cx, dy = y - cy }

paraFromCircle : VectDir -> Circle -> VectDir
paraFromCircle vect disc =
    let
        nvect = normalize vect
        norm = normalize <| perpFromCircle vect disc
        (x,y) = (norm.p.x, norm.p.y)
        r = disc.radius
        point = if lineOrientation (dirToLine vect) (disc.center) == Cw then
                    { x = x + r * norm.dx, y = y + r * norm.dy}
                else
                    {x = x - r * norm.dx, y = y - r * norm.dy}
    in
        { nvect | p = point }

dom : VectDir -> VectDir -> Bool
dom v1 v2 = Ccw /= lineOrientation (dirToLine v1) v2.p

type alias InfList a = (Int, Int, Array.Array a)

mkInf : List a -> InfList a
mkInf xs =
    let
        array = Array.fromList xs
        lastN = Array.length array - 1
    in
        (0, lastN, array)

next : InfList a -> InfList a
next (c,l,a) = if c < l then (c+1,l,a)
               else (0,l,a)

current : InfList a -> Maybe a
current (c,_,a) = Array.get c a

isFirstMin : VectDir -> List (Maybe Line) -> Bool
isFirstMin line list =
    let
        maybeMin l =
            case l of
                [] -> Nothing
                (Nothing :: xs) -> maybeMin xs
                (Just x :: xs) ->
                    case maybeMin xs of
                        Nothing -> Just x
                        Just y -> if dirAngle line (lineToDir x) < dirAngle line (lineToDir y) then Just x else Just y
    in
        case list of
            [] -> False
            (Nothing :: _) -> False
            (Just x :: xs) ->
                case maybeMin (Just x :: xs) of
                    Nothing -> False
                    Just y -> x == y

cut : List Circle -> List Circle
cut list =
    let
        auxCut x y l =
            case l of
                [] -> []
                [z] -> [z]
                (z :: w :: ws) -> if x == z && y == w then []
                                  else z :: auxCut x y (w :: ws)
    in
        case list of
            (x :: y :: xs) -> x :: y :: auxCut x y xs
            xs -> xs

last : List a -> Maybe a
last list =
    case list of
        [] -> Nothing
        [x] -> Just x
        (_ :: xs) -> last xs

init : List a -> Maybe (List a)
init list =
    case list of
        [] -> Nothing
        (x :: xs) ->
            case init xs of
                Nothing -> Just []
                Just ys -> Just (x :: ys)

merge : List Circle -> List Circle -> Maybe (List Circle)
merge hullP hullQ =
    Just (if List.length hullP > 1 then cut hullP else hullP) |> Maybe.andThen (\cutP ->
    Just (if List.length hullQ > 1 then cut hullQ else hullQ) |> Maybe.andThen (\cutQ ->
    List.head cutP |> Maybe.andThen (\hCutP ->
    last cutP |> Maybe.andThen (\lCutP ->
    init cutP |> Maybe.andThen (\iCutP ->
    List.head cutQ |> Maybe.andThen (\hCutQ ->
    last cutQ |> Maybe.andThen (\lCutQ ->
    init cutQ |> Maybe.andThen (\iCutQ ->
    Just (if (hCutP.did == lCutP.did) && (List.length cutP > 1) then iCutP else cutP) |> Maybe.andThen (\hP ->
    Just (if (hCutQ.did == lCutQ.did) && (List.length cutQ > 1) then iCutQ else cutQ) |> Maybe.andThen (\hQ ->
    Just (mkInf hP) |> Maybe.andThen (\p ->
    Just (mkInf hQ) |> Maybe.andThen (\q ->
    current p |> Maybe.andThen (\cp ->
    current q |> Maybe.andThen (\cq ->
    Just (if cp.center.y - cp.radius < cq.center.y - cq.radius then cp else cq) |> Maybe.andThen (\minY ->
    Just (
          { p =
                { x = minY.center.x
                , y = minY.center.y - minY.radius}
          , dx = -1
          , dy = 0 }
         ) |> Maybe.andThen (\l ->
    Just (paraFromCircle l cp) |> Maybe.andThen (\lp ->
    Just (paraFromCircle l cq) |> Maybe.andThen (\lq ->
    mergei [] hP hQ p q l lp lq))))))))))))))))))

type T4 a b c d = T4 a b c d

mergei : List Circle ->
         List Circle ->
         List Circle ->
         InfList Circle ->
         InfList Circle ->
         VectDir ->
         VectDir ->
         VectDir ->
         Maybe (List Circle)
mergei hS lP lQ p q l lp lq =
    case (lP, lQ) of
        ([], []) -> current p |> Maybe.andThen (\cp ->
                    current q |> Maybe.andThen (\cq ->
                    (if dom lp lq then advance (add hS cp) l p q
                     else advance (add hS cq) l q p) |> Maybe.andThen (\(T4 hSi _ _ _) ->
                    Just (List.reverse hSi))))
        (hP,hQ) -> current p |> Maybe.andThen (\cp ->
                   current q |> Maybe.andThen (\cq ->
                   (if dom lp lq then advance (add hS cp) l p q
                    else advance (add hS cq) l q p) |> Maybe.andThen (\(T4 hSi li x y) ->
                   Just (if dom lp lq then (x, y) else (y, x)) |> Maybe.andThen (\(pi, qi) ->
                   current pi |> Maybe.andThen (\cpi ->
                   current qi |> Maybe.andThen (\cqi ->
                   Just (paraFromCircle li cpi, paraFromCircle li cqi) |> Maybe.andThen (\(lpi, lqi) ->
                   Just (
                         case (hP, hQ) of
                             ([], ys) -> ([], remove ys)
                             (xs, []) -> (remove xs, [])
                             (xs, ys) -> if dom lp lq then
                                             (remove xs, ys)
                                         else
                                             (xs, remove ys)
                        ) |> Maybe.andThen (\(hPi, hQi) ->
                   mergei hSi hPi hQi pi qi li lpi lqi))))))))

advance : List Circle ->
          VectDir ->
          InfList Circle ->
          InfList Circle ->
          Maybe (T4 (List Circle) VectDir (InfList Circle) (InfList Circle))
advance hS l x y =
    current x |> Maybe.andThen (\cx ->
    current y |> Maybe.andThen (\cy ->
    current (next x) |> Maybe.andThen (\cnx ->
    current (next y) |> Maybe.andThen (\cny ->
    Just (tangentFromCircleToCircle cx cy) |> Maybe.andThen (\line1 ->
    Just (tangentFromCircleToCircle cx cnx) |> Maybe.andThen (\line2 ->
    Just (tangentFromCircleToCircle cy cny) |> Maybe.andThen (\line3 ->
    Just (tangentFromCircleToCircle cy cx) |> Maybe.andThen (\line4 ->
    Just (if isFirstMin l [line1, line2, line3] then if isFirstMin l [line4, line2, line3] then cx :: (add hS cy) else add hS cy else hS) |> Maybe.andThen (\hSi ->
    (if isFirstMin l [line2, line3] then (line2 |> Maybe.andThen (\l2 -> Just (lineToDir l2, next x, y))) else (case line3 of
                                                                                                                 Nothing -> Just (l, x ,y)
                                                                                                                 Just l3 -> Just (lineToDir l3, x, next y))) |> Maybe.andThen (\(li, xi, yi) ->
    Just (T4 hSi li xi yi)))))))))))

add : List Circle -> Circle -> List Circle
add list d =
    case list of
        [] -> [d]
        (x :: xs) -> if x.did == d.did then
                         x :: xs
                     else
                         d :: x :: xs

remove : List Circle -> List Circle
remove list =
    case list of
        [] -> []
        (_ :: xs)  -> xs

convexCircle : List Circle -> List Circle
convexCircle xs =
    case convexHull xs of
        Nothing -> []
        Just [] -> []
        Just [x] -> [x]
        Just [x,y] -> [x,y]
        Just ys ->
            let
                result = cut ys
            in
                case (List.head result, last result) of
                    (Just hRes, Just lRes) -> if hRes.did == lRes.did then result else result ++ [hRes]
                    _ -> []

convexHull : List Circle -> Maybe (List Circle)
convexHull list =
    case list of
        [] -> Just []
        [x] -> Just [x]
        xs ->
            Just (List.length xs // 2) |> Maybe.andThen (\n ->
            Just (List.take n xs) |> Maybe.andThen (\p ->
            Just (List.drop n xs) |> Maybe.andThen (\q ->
            convexHull p |> Maybe.andThen (\hullP ->
            convexHull q |> Maybe.andThen (\hullQ ->
            merge hullP hullQ)))))

drawHull : List Circle -> List Line
drawHull l =
    let
        auxDraw list =
            case list of
                [] -> []
                [x] -> []
                (x :: y :: xs) ->
                    case tangentFromCircleToCircle x y of
                        Nothing -> auxDraw (y :: xs)
                        Just t -> t :: auxDraw (y :: xs)
    in
        case convexCircle l of
            [] -> []
            [x] -> []
            ys -> auxDraw ys
                             
type alias CircleState =
    { circles : List Circle
    , hull : List Line
    , current : Maybe Circle }

initCircles : CircleState
initCircles =
    { circles = []
    , hull = []
    , current = Nothing }

type alias Model =
    { screen : Screen
    , mouse : Mouse
    , circleState : CircleState
    , play : Bool
    , tick : Int
    , slider : Int}

initial : () -> (Model, Cmd Msg)
initial () =
    ( { screen = toScreen 600 600
      , mouse = initMouse
      , circleState = initCircles
      , play = False
      , tick = 0
      , slider = 1}
    , Task.perform NewViewport Dom.getViewport )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Tick _ -> ( if model.play then
                        let newcircles = move model model.circleState.circles (Random.initialSeed (model.tick+1)) in
                        act { model | circleState =
                                    { circles = newcircles
                                    , current = model.circleState.current
                                    , hull = drawHull newcircles}
                        , tick = model.tick + 1}
                    else
                        { model | tick = model.tick + 1 }
                  , Cmd.none)
        Clear -> ({ model | circleState = initCircles, tick = 0}, Cmd.none)
        Play -> ( { model | play = not model.play
                  , circleState =
                      { circles = List.map (\c -> { c | dest = Nothing }) model.circleState.circles
                      , current = model.circleState.current
                      , hull = model.circleState.hull}}, Cmd.none)
        Slide v -> ( { model | slider = Maybe.withDefault 0 (String.toInt v) }, Cmd.none )
        NewViewport {viewport} ->
            ( (if model.play then
                   (\m -> { m | circleState =
                                  { circles =
                                        List.map
                                        (\c -> if c.center.x - c.radius <= m.screen.left || c.center.x + c.radius >= m.screen.right || c.center.y - c.radius <= m.screen.bottom || c.center.y + c.radius >= m.screen.top then { c | dest = Nothing } else c) m.circleState.circles
                                  , current = m.circleState.current
                                  , hull = m.circleState.hull}}) else act) { model | screen = toScreen viewport.width viewport.height }
            , Cmd.none )
        Resize width height ->
            ( (if model.play then
                   (\m -> { m | circleState =
                                  { circles =
                                        List.map
                                        (\c -> if c.center.x - c.radius <= m.screen.left || c.center.x + c.radius >= m.screen.right || c.center.y - c.radius <= m.screen.bottom || c.center.y + c.radius >= m.screen.top then { c | dest = Nothing } else c) m.circleState.circles
                                  , current = m.circleState.current
                                  , hull = m.circleState.hull}}) else act) { model | screen = toScreen (toFloat width) (toFloat height) }
            , Cmd.none )
        MouseMove x y ->
            ( (if model.play then (\z -> z) else act) { model | mouse =
                              { x = model.screen.left + x
                              , y = model.screen.top - y
                              , down = model.mouse.down } }
            , Cmd.none )
        MouseButton val ->
            ( (if model.play then (\x -> x) else act) { model | mouse =
                              { x = model.mouse.x
                              , y = model.mouse.y
                              , down = val } }
            , Cmd.none )

move : Model -> List Circle -> Random.Seed -> List Circle
move model circles seed =
    case circles of
        [] -> []
        c :: cs ->
            let
                (newCircle, newSeed) = single model c seed
            in
                if newCircle.radius < Basics.min model.screen.right model.screen.top then newCircle :: move model cs newSeed else move model cs newSeed

speed slider = (toFloat slider / 110 + 1 / 11) / 60
                    
single : Model -> Circle -> Random.Seed -> (Circle, Random.Seed)
single model circle seed =
        case circle.dest of
            Nothing ->
                if circle.center.x - circle.radius <= model.screen.left ||
                    circle.center.x + circle.radius >= model.screen.right ||
                    circle.center.y - circle.radius <= model.screen.bottom ||
                    circle.center.y + circle.radius >= model.screen.top then
                    ( { circle | center =
                                   { x = circle.center.x - circle.center.x * speed model.slider
                                   , y = circle.center.y - circle.center.y * speed model.slider}}
                    , seed)
                else
                    let
                        (angle, seedi) = Random.step (Random.float 0 (2 * pi)) seed
                        x = cos angle * 100
                        y = sin angle * 100
                    in
                        ( { circle | dest =
                                       Just
                                       { p = circle.center
                                       , dx = x
                                       , dy = y}
                          , center =
                              { x = circle.center.x + x * speed model.slider
                              , y = circle.center.y + y * speed model.slider}}
                        , seedi)
            Just vect ->
                if (circle.center.x - circle.radius < model.screen.left ||
                    circle.center.x + circle.radius > model.screen.right) &&
                    (circle.center.y - circle.radius < model.screen.bottom ||
                     circle.center.y + circle.radius > model.screen.top) then
                    ( { circle | dest =
                                   Just
                                   { p = circle.center
                                   , dx = -vect.dx
                                   , dy = -vect.dy}
                      , center =
                          { x = circle.center.x + (-vect.dx) * speed model.slider
                          , y = circle.center.y + (-vect.dy) * speed model.slider }}
                    , seed)
                    else
                    if circle.center.x - circle.radius < model.screen.left ||
                    circle.center.x + circle.radius > model.screen.right then
                    ( { circle | dest =
                                   Just
                                   { p = circle.center
                                   , dx = -vect.dx
                                   , dy = vect.dy}
                      , center =
                          { x = circle.center.x + (-vect.dx) * speed model.slider
                          , y = circle.center.y + vect.dy * speed model.slider }}
                    , seed)
                else if circle.center.y - circle.radius < model.screen.bottom ||
                    circle.center.y + circle.radius > model.screen.top then
                         ( { circle | dest =
                                        Just
                                        { p = circle.center
                                        , dx = vect.dx
                                        , dy = -vect.dy}
                           , center =
                               { x = circle.center.x + (vect.dx) * speed model.slider
                               , y = circle.center.y + (-vect.dy) * speed model.slider}}
                         , seed)
                else
                    ( { circle | center =
                                   { x = circle.center.x + vect.dx * speed model.slider
                                   , y = circle.center.y + vect.dy * speed model.slider}}
                    , seed)
            
assignId : List Circle -> Int
assignId xs =
    case xs of
        [] -> 0
        x :: _ -> x.did + 1

distance : Point -> Point -> Float
distance p1 p2 = sqrt ((p2.x-p1.x)^2 + (p2.y-p1.y)^2)

act : Model -> Model
act model =
    let
        left = model.screen.left
        top = model.screen.top
        circles = model.circleState.circles
        currentS = model.circleState.current
        mouseDown = model.mouse.down
        x = model.mouse.x
        y = model.mouse.y
    in
        if x < left + 175 && y > top - 100 then model else
        case (currentS) of
            Nothing -> if mouseDown then
                           { model | circleState =
                                       { circles = circles
                                       , hull = drawHull circles
                                       , current =
                                           Just
                                             { did = assignId circles
                                             , center = { x = x, y = y}
                                             , radius = 0
                                             , dest = Nothing } } }
                       else model
            Just disc -> if mouseDown then
                             let
                                 c = { disc | radius =
                                                distance disc.center
                                                { x = x, y = y } }
                             in
                                 { model | circleState =
                                             { circles = circles
                                             , hull = drawHull (if c.radius > 0 then c :: circles else circles)
                                             , current = Just c } }
                         else if disc.radius > 0 && disc.radius < Basics.min model.screen.right model.screen.top then
                                  { model | circleState =
                                              { circles = disc :: circles
                                              , hull = drawHull (disc :: circles)
                                              , current = Nothing } }
                              else { model | circleState =
                                         { circles = circles
                                         , hull = drawHull circles
                                         , current = Nothing } }

subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ E.onResize Resize
        , E.onMouseMove (D.map2 MouseMove (D.field "pageX" D.float) (D.field "pageY" D.float ) )
        , E.onMouseDown (D.succeed (MouseButton True))
        , E.onMouseUp (D.succeed (MouseButton False))
        , E.onAnimationFrame Tick]

view : Model -> Browser.Document Msg
view model =
    { title = "Convex Hull"
    , body = render model ::
             [ Ht.div [H.style "position" "fixed"]
                   [ Ht.button [He.onClick Clear] [text "Clear"]
                   , Ht.button [He.onClick Play] [text (if model.play then "Pause" else "Play")]
                   , Ht.div [] [text "Speed"]
                   , Ht.div [] [Ht.input [ H.type_ "range"
                               , H.min "1"
                               , H.max "100"
                               , H.value <| String.fromInt model.slider
                               , H.class "slider"
                               , H.id "myRange"
                               , He.onInput Slide ]
                               []] ]]}
    
render : Model -> Svg msg
render model =
    let
        w = String.fromFloat model.screen.width
        h = String.fromFloat model.screen.height
        x = String.fromFloat model.screen.left
        y = String.fromFloat model.screen.bottom
    in
        svg
        [ viewBox (x ++ " " ++ y ++ " " ++ w ++ " " ++ h)
        , H.style "position" "fixed"
        , H.style "top" "0"
        , H.style "left" "0"
        , width "100%"
        , height "100%" ]
        (drawModel model)

drawLine : Line -> Svg msg
drawLine l =
    Svg.line
    [ A.x1 (String.fromFloat l.p1.x)
    , A.y1 (String.fromFloat (-l.p1.y))
    , A.x2 (String.fromFloat l.p2.x)
    , A.y2 (String.fromFloat (-l.p2.y))
    , A.stroke "rgb(87, 130, 255)"
    , A.strokeWidth "2"
    , A.opacity "0.7"] []


drawCircle : Circle -> Svg msg
drawCircle disc =
    Svg.circle
    [ A.cx (String.fromFloat disc.center.x)
    , A.cy (String.fromFloat (-disc.center.y))
    , A.r (String.fromFloat disc.radius)
    , A.stroke "rgb(87, 130, 255)"
    , A.strokeWidth "2"
    , A.fill "rgb(87, 130, 255)"
    , A.fillOpacity "0.6"] []

drawPointer : Model -> Svg msg
drawPointer model =
    Svg.circle
    [ A.cx (String.fromFloat model.mouse.x)
    , A.cy (String.fromFloat (-model.mouse.y))
    , A.r "5"
    , A.fill "rgb(87, 130, 255)"] []

drawCurrent : Maybe Circle -> Maybe (Svg msg)
drawCurrent maybeCircle =
    case maybeCircle of
        Nothing -> Nothing
        Just disc -> Just <|
                     Svg.circle
                     [ A.cx (String.fromFloat disc.center.x)
                     , A.cy (String.fromFloat (-disc.center.y))
                     , A.r (String.fromFloat disc.radius)
                     , A.stroke "rgb(87, 130, 255)"
                     , A.strokeWidth "2"
                     , A.fill "rgb(87, 130, 255)"
                     , A.fillOpacity "0.2"] []

drawModel : Model -> List (Svg msg)
drawModel model =
    case drawCurrent model.circleState.current of
        Nothing ->
            (drawBackground model :: drawPointer model ::
             List.map drawCircle model.circleState.circles)
            ++ List.map drawLine model.circleState.hull
        Just svg -> (drawBackground model ::
                     svg :: drawPointer model ::
                     List.map drawCircle model.circleState.circles)
                    ++ List.map drawLine model.circleState.hull

drawBackground : Model -> Svg msg
drawBackground model =
    Svg.rect
    [ A.x <| String.fromFloat model.screen.left
    , A.y <| String.fromFloat model.screen.bottom
    , A.width <| String.fromFloat model.screen.width
    , A.height <| String.fromFloat model.screen.height
    , A.fill "rgb(66, 66, 105 )"] []
